import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FixedCallToActionComponent } from './fixed-call-to-action.component';

describe('FixedCallToActionComponent', () => {
  let component: FixedCallToActionComponent;
  let fixture: ComponentFixture<FixedCallToActionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FixedCallToActionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FixedCallToActionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
