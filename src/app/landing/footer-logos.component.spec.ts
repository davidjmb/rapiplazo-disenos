import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FooterLogosComponent } from './footer-logos.component';

describe('FooterLogosComponent', () => {
  let component: FooterLogosComponent;
  let fixture: ComponentFixture<FooterLogosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FooterLogosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FooterLogosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
