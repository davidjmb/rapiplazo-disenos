import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConfirmarCuentaBancariaComponent } from './confirmar-cuenta-bancaria.component';

describe('ConfirmarCuentaBancariaComponent', () => {
  let component: ConfirmarCuentaBancariaComponent;
  let fixture: ComponentFixture<ConfirmarCuentaBancariaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConfirmarCuentaBancariaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfirmarCuentaBancariaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
