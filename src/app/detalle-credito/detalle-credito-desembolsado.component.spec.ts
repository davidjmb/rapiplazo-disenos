import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DetalleCreditoDesembolsadoComponent } from './detalle-credito-desembolsado.component';

describe('DetalleCreditoDesembolsadoComponent', () => {
  let component: DetalleCreditoDesembolsadoComponent;
  let fixture: ComponentFixture<DetalleCreditoDesembolsadoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetalleCreditoDesembolsadoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetalleCreditoDesembolsadoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
