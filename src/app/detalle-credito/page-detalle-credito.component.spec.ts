import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PageDetalleCreditoComponent } from './page-detalle-credito.component';

describe('PageDetalleCreditoComponent', () => {
  let component: PageDetalleCreditoComponent;
  let fixture: ComponentFixture<PageDetalleCreditoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PageDetalleCreditoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PageDetalleCreditoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
