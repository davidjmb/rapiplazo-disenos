import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HistorialCreditoComponent } from './historial-credito.component';

describe('HistorialCreditoComponent', () => {
  let component: HistorialCreditoComponent;
  let fixture: ComponentFixture<HistorialCreditoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HistorialCreditoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HistorialCreditoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
