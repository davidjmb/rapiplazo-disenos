import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PopupEnEfectivoComponent } from './popup-en-efectivo.component';

describe('PopupEnEfectivoComponent', () => {
  let component: PopupEnEfectivoComponent;
  let fixture: ComponentFixture<PopupEnEfectivoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PopupEnEfectivoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PopupEnEfectivoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
