import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PopupTransferenciaBancariaComponent } from './popup-transferencia-bancaria.component';

describe('PopupTransferenciaBancariaComponent', () => {
  let component: PopupTransferenciaBancariaComponent;
  let fixture: ComponentFixture<PopupTransferenciaBancariaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PopupTransferenciaBancariaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PopupTransferenciaBancariaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
