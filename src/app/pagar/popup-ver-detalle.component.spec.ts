import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PopupVerDetalleComponent } from './popup-ver-detalle.component';

describe('PopupVerDetalleComponent', () => {
  let component: PopupVerDetalleComponent;
  let fixture: ComponentFixture<PopupVerDetalleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PopupVerDetalleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PopupVerDetalleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
