import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PopupCuentaAhorros2Component } from './popup-cuenta-ahorros2.component';

describe('PopupCuentaAhorros2Component', () => {
  let component: PopupCuentaAhorros2Component;
  let fixture: ComponentFixture<PopupCuentaAhorros2Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PopupCuentaAhorros2Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PopupCuentaAhorros2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
