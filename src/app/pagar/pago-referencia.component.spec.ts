import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PagoReferenciaComponent } from './pago-referencia.component';

describe('PagoReferenciaComponent', () => {
  let component: PagoReferenciaComponent;
  let fixture: ComponentFixture<PagoReferenciaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PagoReferenciaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PagoReferenciaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
