import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PopupCuentaAhorrosComponent } from './popup-cuenta-ahorros.component';

describe('PopupCuentaAhorrosComponent', () => {
  let component: PopupCuentaAhorrosComponent;
  let fixture: ComponentFixture<PopupCuentaAhorrosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PopupCuentaAhorrosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PopupCuentaAhorrosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
