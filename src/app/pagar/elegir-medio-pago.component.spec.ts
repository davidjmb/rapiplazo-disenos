import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ElegirMedioPagoComponent } from './elegir-medio-pago.component';

describe('ElegirMedioPagoComponent', () => {
  let component: ElegirMedioPagoComponent;
  let fixture: ComponentFixture<ElegirMedioPagoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ElegirMedioPagoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ElegirMedioPagoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
