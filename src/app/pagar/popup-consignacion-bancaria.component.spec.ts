import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PopupConsignacionBancariaComponent } from './popup-consignacion-bancaria.component';

describe('PopupConsignacionBancariaComponent', () => {
  let component: PopupConsignacionBancariaComponent;
  let fixture: ComponentFixture<PopupConsignacionBancariaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PopupConsignacionBancariaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PopupConsignacionBancariaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
