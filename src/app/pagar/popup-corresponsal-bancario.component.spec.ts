import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PopupCorresponsalBancarioComponent } from './popup-corresponsal-bancario.component';

describe('PopupCorresponsalBancarioComponent', () => {
  let component: PopupCorresponsalBancarioComponent;
  let fixture: ComponentFixture<PopupCorresponsalBancarioComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PopupCorresponsalBancarioComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PopupCorresponsalBancarioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
