import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PopupCajerosAutomaticosComponent } from './popup-cajeros-automaticos.component';

describe('PopupCajerosAutomaticosComponent', () => {
  let component: PopupCajerosAutomaticosComponent;
  let fixture: ComponentFixture<PopupCajerosAutomaticosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PopupCajerosAutomaticosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PopupCajerosAutomaticosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
