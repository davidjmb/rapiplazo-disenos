import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ZonaGrisComponent } from './zona-gris.component';

describe('ZonaGrisComponent', () => {
  let component: ZonaGrisComponent;
  let fixture: ComponentFixture<ZonaGrisComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ZonaGrisComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ZonaGrisComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
