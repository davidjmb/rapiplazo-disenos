import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SolicitudZonaGrisComponent } from './solicitud-zona-gris.component';

describe('SolicitudZonaGrisComponent', () => {
  let component: SolicitudZonaGrisComponent;
  let fixture: ComponentFixture<SolicitudZonaGrisComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SolicitudZonaGrisComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SolicitudZonaGrisComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
