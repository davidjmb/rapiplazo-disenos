import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TuAceptacionComponent } from './tu-aceptacion.component';

describe('TuAceptacionComponent', () => {
  let component: TuAceptacionComponent;
  let fixture: ComponentFixture<TuAceptacionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TuAceptacionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TuAceptacionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
