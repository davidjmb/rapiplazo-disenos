import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AceptarOferta2Component } from './aceptar-oferta2.component';

describe('AceptarOferta2Component', () => {
  let component: AceptarOferta2Component;
  let fixture: ComponentFixture<AceptarOferta2Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AceptarOferta2Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AceptarOferta2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
