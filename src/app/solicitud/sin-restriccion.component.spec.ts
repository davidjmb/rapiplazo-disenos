import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SinRestriccionComponent } from './sin-restriccion.component';

describe('SinRestriccionComponent', () => {
  let component: SinRestriccionComponent;
  let fixture: ComponentFixture<SinRestriccionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SinRestriccionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SinRestriccionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
