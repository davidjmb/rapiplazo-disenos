import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SolicitudSinRestriccionComponent } from './solicitud-sin-restriccion.component';

describe('SolicitudSinRestriccionComponent', () => {
  let component: SolicitudSinRestriccionComponent;
  let fixture: ComponentFixture<SolicitudSinRestriccionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SolicitudSinRestriccionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SolicitudSinRestriccionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
