import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AceptacionCredito2Component } from './aceptacion-credito2.component';

describe('AceptacionCredito2Component', () => {
  let component: AceptacionCredito2Component;
  let fixture: ComponentFixture<AceptacionCredito2Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AceptacionCredito2Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AceptacionCredito2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
