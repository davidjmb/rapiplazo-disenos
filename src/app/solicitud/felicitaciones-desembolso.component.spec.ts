import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FelicitacionesDesembolsoComponent } from './felicitaciones-desembolso.component';

describe('FelicitacionesDesembolsoComponent', () => {
  let component: FelicitacionesDesembolsoComponent;
  let fixture: ComponentFixture<FelicitacionesDesembolsoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FelicitacionesDesembolsoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FelicitacionesDesembolsoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
