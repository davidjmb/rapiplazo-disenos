import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NecesitamosTuAceptacionComponent } from './necesitamos-tu-aceptacion.component';

describe('NecesitamosTuAceptacionComponent', () => {
  let component: NecesitamosTuAceptacionComponent;
  let fixture: ComponentFixture<NecesitamosTuAceptacionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NecesitamosTuAceptacionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NecesitamosTuAceptacionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
