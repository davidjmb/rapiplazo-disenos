import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AceptacionCreditoComponent } from './aceptacion-credito.component';

describe('AceptacionCreditoComponent', () => {
  let component: AceptacionCreditoComponent;
  let fixture: ComponentFixture<AceptacionCreditoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AceptacionCreditoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AceptacionCreditoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
