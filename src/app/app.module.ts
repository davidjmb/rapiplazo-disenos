import { BrowserModule } from '@angular/platform-browser';
import {LOCALE_ID, NgModule} from '@angular/core';
import 'hammerjs';
import {registerLocaleData} from '@angular/common';
import {AppRoutingModule} from './app-routing.module';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {RouterModule, Routes} from '@angular/router';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {
  MatButtonModule,
  MatCardModule,
  MatCheckboxModule,
  MatExpansionModule,
  MatIconModule,
  MatInputModule,
  MatListModule,
  MatOptionModule,
  MatPaginatorModule,
  MatSelectModule,
  MatSidenavModule,
  MatSliderModule,
  MatTableModule,
  MatToolbarModule,
  MatFormFieldModule,
  MatTooltipModule,
  MatNativeDateModule,
  MatMenuModule,
  MatProgressBarModule,
  MatDialogModule,
  MatTabsModule,
  MatRadioModule,
} from '@angular/material';
import localeEs from '@angular/common/locales/es-CO';
import {FormlyModule} from '@ngx-formly/core';
import {FormlyMaterialModule} from '@ngx-formly/material';
import { CommonModule } from '@angular/common';
import {FormlyMatDatepickerModule} from '@ngx-formly/material/datepicker';
import {MatDatepickerModule} from '@angular/material';
import {MatMomentDateModule} from '@angular/material-moment-adapter';
import { FormlyMatNativeSelectModule } from '@ngx-formly/material/native-select';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { SolicitudComponent } from './solicitud/solicitud.component';
import { ProgresoComponent } from './solicitud/progreso/progreso.component';
import { FormularioComponent } from './solicitud/formulario/formulario.component';
import { LandingComponent } from './landing/landing.component';
import { HeaderComponent } from './landing/header.component';
import { SidenavComponent } from './landing/sidenav.component';
import { SimuladorComponent } from './landing/simulador.component';
import { JumbotronComponent } from './landing/jumbotron.component';
import { ComoFuncionaComponent } from './landing/como-funciona.component';
import { CallToActionComponent } from './landing/call-to-action.component';
import { AliadosComponent } from './landing/aliados.component';
import { FooterComponent } from './landing/footer.component';
import { FooterLogosComponent } from './landing/footer-logos.component';
import { FixedCallToActionComponent } from './landing/fixed-call-to-action.component';
import { PopupsComponent } from './popups/popups.component';
import { InfoSolicitudComponent } from './solicitud/info-solicitud.component';
import { AceptacionCreditoComponent } from './solicitud/aceptacion-credito.component';
import { AceptarOfertaComponent } from './solicitud/aceptar-oferta.component';
import { AceptacionCredito2Component } from './solicitud/aceptacion-credito2.component';
import { AceptarOferta2Component } from './solicitud/aceptar-oferta2.component';
import { RechazoSolicitudComponent } from './solicitud/rechazo-solicitud.component';
import { SolicitudRechazadaComponent } from './solicitud/solicitud-rechazada.component';
import { SolicitudZonaGrisComponent } from './solicitud/solicitud-zona-gris.component';
import { ZonaGrisComponent } from './solicitud/zona-gris.component';
import { CreditosEnProcesoComponent } from './home/creditos-en-proceso.component';
import { NecesitamosTuAceptacionComponent } from './solicitud/necesitamos-tu-aceptacion.component';
import { TuAceptacionComponent } from './solicitud/tu-aceptacion.component';
import { FelicitacionesDesembolsoComponent } from './solicitud/felicitaciones-desembolso.component';
import { DesembolsoComponent } from './solicitud/desembolso.component';
import { ConfirmaInformacionComponent } from './confirma-informacion/confirma-informacion.component';
import { ConfirmarCuentaBancariaComponent } from './confirma-informacion/confirmar-cuenta-bancaria.component';
import { SolicitudSinRestriccionComponent } from './solicitud/solicitud-sin-restriccion.component';
import { SinRestriccionComponent } from './solicitud/sin-restriccion.component';
import { PageDetalleCreditoComponent } from './detalle-credito/page-detalle-credito.component';
import { DetalleCreditoDesembolsadoComponent } from './detalle-credito/detalle-credito-desembolsado.component';
import { HistorialCrediticioComponent } from './historial-crediticio/historial-crediticio.component';
import { HistorialCreditoComponent } from './historial-crediticio/historial-credito.component';
import { ElegirMedioPagoComponent } from './pagar/elegir-medio-pago.component';
import { PagoReferenciaComponent } from './pagar/pago-referencia.component';
import { MediosPagoComponent } from './pagar/medios-pago.component';
import { PopupVerDetalleComponent } from './pagar/popup-ver-detalle.component';
import { PopupCuentaAhorrosComponent } from './pagar/popup-cuenta-ahorros.component';
import { PopupCuentaAhorros2Component } from './pagar/popup-cuenta-ahorros2.component';
import { PopupEnEfectivoComponent } from './pagar/popup-en-efectivo.component';
import { PopupTransferenciaBancariaComponent } from './pagar/popup-transferencia-bancaria.component';
import { PopupConsignacionBancariaComponent } from './pagar/popup-consignacion-bancaria.component';
import { PopupCajerosAutomaticosComponent } from './pagar/popup-cajeros-automaticos.component';
import { PopupCorresponsalBancarioComponent } from './pagar/popup-corresponsal-bancario.component';

registerLocaleData(localeEs, 'es-CO'); // DA FORMATO A LOS PIPE EN LOS SERVICIOS y proporciona la zona horaria correcta
const routes: Routes = [
  {path: '', redirectTo: '/', pathMatch: 'full'},
  {path: 'home', component: HomeComponent},
  {path: 'solicitud', component: SolicitudComponent},
  {path: 'solicitud/aceptacion-credito', component: AceptacionCreditoComponent},
  {path: 'solicitud/necesitamos-tu-aceptacion', component: NecesitamosTuAceptacionComponent},
  {path: 'solicitud/felicitaciones-desembolso', component: FelicitacionesDesembolsoComponent},
  {path: 'solicitud/aceptacion-credito-2', component: AceptacionCredito2Component},
  {path: 'solicitud/rechazo-solicitud', component: RechazoSolicitudComponent},
  {path: 'solicitud/zona-gris', component: SolicitudZonaGrisComponent},
  {path: 'solicitud/sin-restriccion', component: SolicitudSinRestriccionComponent},
  {path: 'confirmar-informacion/cuenta-bancaria', component: ConfirmaInformacionComponent},
  {path: 'historial-crediticio', component: HistorialCrediticioComponent},
  {path: 'landing', component: LandingComponent},
  {path: 'pagar', component: ElegirMedioPagoComponent},
  {path: 'pagar/popup-cuenta-ahorros', component: PopupCuentaAhorrosComponent},
  {path: 'pagar/popup-cuenta-ahorros2', component: PopupCuentaAhorros2Component},
  {path: 'pagar/popup-en-efectivo', component: PopupEnEfectivoComponent},
  {path: 'pagar/popup-transferencia-bancaria', component: PopupTransferenciaBancariaComponent},
  {path: 'pagar/popup-consignacion-bancaria', component: PopupConsignacionBancariaComponent},
  {path: 'pagar/popup-cajeros-automaticos', component: PopupCajerosAutomaticosComponent},
  {path: 'pagar/popup-corresponsal-bancario', component: PopupCorresponsalBancarioComponent},
  {path: 'pagar/popup-ver-detalle', component: PopupVerDetalleComponent},
  {path: 'detalle-credito', component: PageDetalleCreditoComponent},
  {path: 'popups', component: PopupsComponent},
  // {path: 'home', component: HomeComponent},
  // {path: 'simulator', component: SimulatorComponent},
  // {path: 'table', component: TableExampleComponent},
  // {path: 'validateInformation', component: ValidateInformationComponent},
  {path: '**', redirectTo: '/', pathMatch: 'full'},
];

export function minlengthValidationMessage(err, field) {
  return `Should have atleast ${field.templateOptions.minLength} characters`;
}

export function maxlengthValidationMessage(err, field) {
  return `This value should be less than ${field.templateOptions.maxLength} characters`;
}

export function patternlengthValidationMessage(err, field) {
  return `message: ${field.validation.messages.pattern}`;
}

export function minValidationMessage(err, field) {
  return `This value should be more than ${field.templateOptions.min}`;
}

export function maxValidationMessage(err, field) {
  return `This value should be less than ${field.templateOptions.max}`;
}


@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    SolicitudComponent,
    ProgresoComponent,
    FormularioComponent,
    LandingComponent,
    HeaderComponent,
    SidenavComponent,
    SimuladorComponent,
    JumbotronComponent,
    ComoFuncionaComponent,
    CallToActionComponent,
    AliadosComponent,
    FooterComponent,
    FooterLogosComponent,
    FixedCallToActionComponent,
    PopupsComponent,
    InfoSolicitudComponent,
    AceptacionCreditoComponent,
    AceptarOfertaComponent,
    AceptacionCredito2Component,
    AceptarOferta2Component,
    RechazoSolicitudComponent,
    SolicitudRechazadaComponent,
    SolicitudZonaGrisComponent,
    ZonaGrisComponent,
    CreditosEnProcesoComponent,
    NecesitamosTuAceptacionComponent,
    TuAceptacionComponent,
    FelicitacionesDesembolsoComponent,
    DesembolsoComponent,
    ConfirmaInformacionComponent,
    ConfirmarCuentaBancariaComponent,
    SolicitudSinRestriccionComponent,
    SinRestriccionComponent,
    PageDetalleCreditoComponent,
    DetalleCreditoDesembolsadoComponent,
    HistorialCrediticioComponent,
    HistorialCreditoComponent,
    ElegirMedioPagoComponent,
    PagoReferenciaComponent,
    MediosPagoComponent,
    PopupVerDetalleComponent,
    PopupCuentaAhorrosComponent,
    PopupCuentaAhorros2Component,
    PopupEnEfectivoComponent,
    PopupTransferenciaBancariaComponent,
    PopupConsignacionBancariaComponent,
    PopupCajerosAutomaticosComponent,
    PopupCorresponsalBancarioComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    FormsModule,
    RouterModule.forRoot(routes),
    BrowserAnimationsModule,
    MatIconModule,
    MatToolbarModule,
    MatSidenavModule,
    MatListModule,
    MatButtonModule,
    MatExpansionModule,
    MatCardModule,
    MatSliderModule,
    MatOptionModule,
    MatSelectModule,
    MatFormFieldModule,
    MatInputModule,
    ReactiveFormsModule,
    MatCheckboxModule,
    MatPaginatorModule,
    MatTableModule,
    MatTooltipModule,
    MatMenuModule,
    MatCheckboxModule,
	MatProgressBarModule,
	MatDialogModule,
	MatTabsModule,
	MatRadioModule,
    FormlyModule.forRoot({
      validationMessages: [
        { name: 'required', message: 'This field is required' },
        { name: 'minlength', message: minlengthValidationMessage },
        { name: 'maxlength', message: maxlengthValidationMessage },
        { name: 'min', message: minValidationMessage },
        { name: 'max', message: maxValidationMessage },
        { name: 'pattern', message: patternlengthValidationMessage },
        {name: 'server-error', message: (err) => err },

      ],

    }),
    FormlyMaterialModule,
    MatNativeDateModule,
    FormlyMatDatepickerModule,
    CommonModule,
    MatDatepickerModule,
    MatMomentDateModule,
    FormlyMatNativeSelectModule,
  ],
  providers: [
    {provide: LOCALE_ID, useValue: 'es-CO'}, // DA FORMATO A LOS PIPE EN EL HTML
    // {provide: HTTP_INTERCEPTORS, useClass: TokenInterceptor, multi: true},
    // {provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true}
  ],
  bootstrap: [AppComponent]
})

export class AppModule { }
